import React, { Component } from 'react'
import './App.css'
import axios from 'axios'
import ReactModal from 'react-modal'
import MDSpinner from 'react-md-spinner'
import MainContainer from './MainContainer/MainContainer'

const Loader = ({ isLoading }) => (
  <div className={`loader ${isLoading ? 'fadeIn' : 'fadeOut'}`}>
    <MDSpinner className="loader-animation" />
  </div>
)

const Errors = ({ errors }) => (
  <div className="error-container">
    <p>
      {errors}
    </p>
  </div>
)
const url = 'http://localhost:4000'

class App extends Component {
  constructor (props) {
    super(props)

    this.state = {
      users: [],
      countriesIso2: [],
      isLoading: true,
      errors: ''
    }

    this.addNewUser = this.addNewUser.bind(this)
    this.updateUser = this.updateUser.bind(this)
    this.deleteUser = this.deleteUser.bind(this)
  }

  componentDidMount () {
    axios.get(`${url}/v1/users`)
      .then((response) => {
        this.setState({
          users: response.data.users
        })
        this.timeoutHandle = setTimeout(() => {
          this.setState({
            isLoading: false
          })
        }, 1500)
      })
      .catch((response) => {
        this.setState({
          errors: 'Something went wrong while loading users. Please contact the administrator.'
        })
        this.timeoutHandle = setTimeout(() => {
          this.setState({
            isLoading: false
          })
        }, 1500)
      })

    axios.get(`${url}/v1/locations/countries`)
      .then((response) => {
        this.setState({
          countriesIso2: response.data
        })
      })
      .catch((response) => {
        this.setState({
          errors: 'Something went wrong while loading Locations. Please contact the administrator.'
        })
      })

    if (typeof (window) !== 'undefined') {
      ReactModal.setAppElement('body')
    }
  }

  addNewUser (user, closeModal) {
    const { users } = this.state

    axios.post(`${url}/v1/users`, { user })
      .then((response) => {
        const newUsers = [...users, response.data]
        this.setState({
          users: newUsers
        })
        closeModal()
      })
      .catch((response) => {
        this.setState({
          errors: 'Something went wrong while creating User. Please contact the administrator.'
        })
      })
  }

  updateUser (id, user, closeModal) {
    const { users } = this.state

    axios.put(`${url}/v1/users/${user.id}`, { user })
      .then((response) => {
        users[id] = response.data
        this.setState({
          users
        })
        closeModal()
      })
      .catch((response) => {
        this.setState({
          errors: 'Something went wrong while updating User. Please contact the administrator.'
        })
      })
  }

  deleteUser (userId, user) {
    const { users } = this.state

    axios.delete(`${url}/v1/users/${user.id}`, { user })
      .then((response) => {
        users.splice(userId, 1)
        this.setState({
          users
        })
      })
      .catch((response) => {
        this.setState({
          errors: 'Something went wrong while deleting User. Please contact the administrator.'
        })
      })
  }

  render () {
    const { addNewUser, updateUser, deleteUser } = this
    const {
      users, countriesIso2, isLoading, errors
    } = this.state

    return (
      <div className="App">
        <Loader isLoading={isLoading} />
        <header className="App-header">
          <h1 className="App-title" alt="Timing Timer">
Timing Timer
            {' '}
          </h1>
        </header>

        <MainContainer
          users={users}
          errors={errors}
          url={url}
          errorsContainer={<Errors errors={errors} />}
          countriesIso2={countriesIso2}
          addNewUser={addNewUser}
          updateUser={updateUser}
          deleteUser={deleteUser}
        />

      </div>
    )
  }
}

export default App
