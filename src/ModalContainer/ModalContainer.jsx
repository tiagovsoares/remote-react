import React from 'react'
import ReactModal from 'react-modal'
import './ModalContainer.css'
import NewUserContainer from '../NewUserContainer/NewUserContainer'
import UpdateUserContainer from '../UpdateUserContainer/UpdateUserContainer'

const ModalContainer = (props) => {
  const {
    addNewUser, updateUser, modalIsOpen, closeModal, modalType, config, users, countriesIso2, citiesForSelect, getCitiesFromCountry, errors
  } = props

  return (
    <div>
      <ReactModal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        className="Modal"
        overlayClassName="Overlay"
      >
        {modalType === 'newUser' && (
          <NewUserContainer
            submitNewUser={addNewUser}
            countriesIso2={countriesIso2}
            citiesForSelect={citiesForSelect}
            getCitiesFromCountry={getCitiesFromCountry}
            closeModal={closeModal}
            errors={errors}
          />
        )}
        {modalType === 'editUser' && (
          <UpdateUserContainer
            updateUser={updateUser}
            countriesIso2={countriesIso2}
            citiesForSelect={citiesForSelect}
            getCitiesFromCountry={getCitiesFromCountry}
            config={config}
            users={users}
            closeModal={closeModal}
            errors={errors}
          />
        )}
      </ReactModal>
    </div>
  )
}

export default ModalContainer
