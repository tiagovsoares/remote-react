import React, { Component } from 'react'
import './UpdateUserContainer.css'
import _ from 'lodash'
import Select from 'react-select'
import moment from 'moment-timezone'
import User from '../UserContainer/UserContainer'

class UpdateUserContainer extends Component {
  constructor (props) {
    super(props)
    const { config, users } = props
    this.state = {
      inputs: users[config]
    }

    this.updateUserField = this.updateUserField.bind(this)
    this.submitButton = this.submitButton.bind(this)
  }

  componentDidMount () {
    const { inputs } = this.state
    const { getCitiesFromCountry } = this.props
    getCitiesFromCountry(inputs.country)
  }

  updateUserField (field, value, getCitiesFromCountry) {
    const { inputs } = this.state
    const newInputs = Object.assign({}, inputs, { [field]: value })

    this.setState({
      inputs: newInputs
    })
    if (getCitiesFromCountry) {
      (
        getCitiesFromCountry(newInputs.country)
      )
    }
  }

  submitButton () {
    const { updateUser, closeModal, config } = this.props
    const { inputs } = this.state

    updateUser(config, inputs, closeModal)

    this.setState({
      inputs: {
        name: '',
        image: '',
        country: '',
        city: ''
      }
    })
  }

  render () {
    const {
      countriesIso2, citiesForSelect, getCitiesFromCountry, errors
    } = this.props
    const { inputs } = this.state
    const {
      name, image, country, city
    } = inputs
    const countriesForSelect = _.map(countriesIso2.locations,
      countriesList => ({ value: countriesList.iso2, label: countriesList.country }))
    const countryValue = _.find(countriesForSelect, { value: country })
    const citiesForSelectFinal = _.map(citiesForSelect,
      city => ({ value: city.city, label: city.city }))
    const cityValue = _.find(citiesForSelectFinal, { value: city })
    const valid = _.every(_.values(inputs), val => val !== '')

    return (
      <div className="UpdateUserContainer">
        <User
          key={name}
          {...inputs}
          currentMoment={moment()}
          className="w-100"
        />

        {errors}

        <input
          className="edit-user-input"
          onChange={e => this.updateUserField('name', e.target.value)}
          value={name}
          placeholder="Colleague's name"
          type="text"
        />

        <input
          className="edit-user-input"
          onChange={e => this.updateUserField('image', e.target.value)}
          value={image}
          placeholder="Profile Image (link)"
          type="text"
        />

        <Select
          value={countryValue}
          onChange={({ value }) => this.updateUserField('country', value, getCitiesFromCountry)}
          options={countriesForSelect}
          placeholder="Select country"
          className="new-user-select-input"
        />
        <Select
          value={cityValue}
          onChange={({ value }) => this.updateUserField('city', value)}
          options={citiesForSelectFinal}
          placeholder="Select city"
          className="new-user-select-input"
        />

        <button
          className="edit-user-button"
          type="button"
          disabled={!valid}
          onClick={e => this.submitButton()}
        >
        Update User
        </button>
      </div>
    )
  }
}

export default UpdateUserContainer
