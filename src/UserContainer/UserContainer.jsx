import './UserContainer.css'
import React from 'react'
import moment from 'moment-timezone'

const Time = ({ userMoment }) => (
  <div>
    <div className="user-info-text">
      <strong>
        {userMoment.format('HH')}
      </strong>
:
      {userMoment.format('mm')}
      <small>
:
        {userMoment.format('ss')}
      </small>
    </div>
    <div className="user-info-sub-text">
      {userMoment.format('ddd')}
    </div>
  </div>
)

const Location = ({ country, city }) => (
  <div className="user-info-text">
    <strong>
      {city || 'City'}
    </strong>
    {', '}
    <small>
      {country || 'Country'}
    </small>
  </div>
)

const UserName = ({ name }) => (
  <div className="user-info-sub-text">
    {name || 'Name'}
  </div>
)

const UserImage = ({ image }) => (
  <div className="profile-image-container">
    <img alt="Profile" className="profile-image" src={image || 'https://bit.ly/2Ri65rw'} />
  </div>
)

const Actions = ({ editUser, deleteUser }) => (
  <div className="actions-container action-section">
    <button type="button" className="lnr lnr-trash" onClick={e => deleteUser()} />
    <button type="button" className="lnr lnr-pencil" onClick={e => editUser()} />
  </div>
)

const User = (props) => {
  const {
    name, image, country, city, timezone, currentMoment, showActions, editUser, deleteUser
  } = props
  const userMoment = currentMoment.tz(timezone) || moment()

  return (
    <div className="user-container">
      <UserImage image={image} />
      <div className="user-info-container">
        <Time userMoment={userMoment} />
      </div>
      <div className="location-container">
        <Location city={city} country={country} />
        {timezone}
        <UserName name={name} />
      </div>
      {showActions && <Actions className="actions-container" editUser={editUser} deleteUser={deleteUser} />}
    </div>
  )
}

export default User
