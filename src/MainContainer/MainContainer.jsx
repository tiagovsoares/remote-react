import React, { Component } from 'react'
import _ from 'lodash'
import moment from 'moment-timezone'
import './MainContainer.css'
import axios from 'axios'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlusCircle, faUsersCog } from '@fortawesome/free-solid-svg-icons'
import User from '../UserContainer/UserContainer'
import ModalContainer from '../ModalContainer/ModalContainer'

class MainContainer extends Component {
  constructor (props) {
    super(props)

    this.state = {
      time: moment(),
      showActions: false,
      modal: {
        show: false,
        type: null,
        config: null
      }
    }

    this.refreshTime = this.refreshTime.bind(this)
    this.toggleActionButtons = this.toggleActionButtons.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.showModal = this.showModal.bind(this)
    this.getCitiesFromCountry = this.getCitiesFromCountry.bind(this)
  }

  componentWillMount () {
    setInterval(() => this.refreshTime(), 1000)
  }

  getCitiesFromCountry (iso2) {
    const { url } = this.props
    axios.get(`${url}/v1/locations/cities/${iso2}`)
      .then((response) => {
        this.setState({
          citiesForSelect: response.data.locations
        })
      })
      .catch((response) => {
        console.log(response)
      })
  }

  refreshTime () {
    const { modal } = this.state
    if (!modal.show) {
      (
        this.setState({
          time: moment()
        })
      )
    }
  }

  closeModal () {
    this.setState({
      modal: {
        show: false
      }
    })
  }

  showModal (type, config) {
    this.setState({
      modal: {
        type,
        show: true,
        config
      }
    })
  }

  toggleActionButtons () {
    this.setState(prevState => ({ showActions: !prevState.showActions }))
  }

  render () {
    const {
      users, countriesIso2, addNewUser, updateUser, deleteUser, errors, errorsContainer
    } = this.props
    const {
      time, showActions, modal, citiesForSelect
    } = this.state
    const {
      toggleActionButtons, closeModal, showModal, getCitiesFromCountry
    } = this

    return (
      <div className="main-container">

        <div className="action-buttons">
          <button type="button" onClick={e => showModal('newUser')}>
            <FontAwesomeIcon icon={faPlusCircle} />
          </button>
          <button type="button" onClick={e => toggleActionButtons()}>
            <FontAwesomeIcon icon={faUsersCog} />
          </button>
        </div>
        {errors && errorsContainer}
        {_.map(users, (user, index) => (
          <User
            key={index}
            id={user.id}
            {...user}
            currentMoment={time}
            editUser={e => showModal('editUser', index)}
            deleteUser={e => deleteUser(index, user)}
            showActions={showActions}
          />
        ))}

        <ModalContainer
          addNewUser={addNewUser}
          updateUser={updateUser}
          modalIsOpen={modal.show}
          modalType={modal.type}
          users={users}
          countriesIso2={countriesIso2}
          citiesForSelect={citiesForSelect}
          getCitiesFromCountry={getCitiesFromCountry}
          config={modal.config}
          closeModal={closeModal}
          errors={errors && errorsContainer}
        />

      </div>
    )
  }
}

export default MainContainer
