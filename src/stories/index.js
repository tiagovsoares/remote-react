import React from 'react';
import moment from "moment-timezone";

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import { addDecorator } from '@storybook/react'; // <- or your storybook framework
import { withBackgrounds } from '@storybook/addon-backgrounds';
import MainContainer from '../MainContainer/MainContainer';
import UserContainer from '../UserContainer/UserContainer';


// addDecorator(
//   withBackgrounds([
//     { name: 'Light blue', value: '#00aced', default: true },
//     { name: 'Dark Blue', value: '#3b5998' },
//     { name: 'Dark Blue', value: 'rgb(50, 64, 80)' },
//   ])
// );

  storiesOf('UI', module)
  .addParameters({
    backgrounds: [
      { name: 'red', value: '#F44336' },
      { name: 'blue', value: '#2196F3', default: true },
    ],
  })
  .add('Main Container', () => (
        <MainContainer users={[
      {name: 'Tim', country: 'AU', city: 'Melbourne'},
      {name: 'James', country: 'JE', city: 'St. Helier', timezone: 'Europe/London'},
      {name: 'Tiago', country: 'PT', city: 'Funchal'}]} />
  ))
  .add('User Container', () => (
    <UserContainer {...{name: 'Tim', country: 'AU', city: 'Melbourne', currentMoment: moment()}} />
  ));
